package com.jass.e_swift.DbHelpar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class SQLHelper extends SQLiteOpenHelper {
    Context context;
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ESWIFT.db";


    //----------------------- Table Name------------------------------------------------
    static final String TOKAN_TABLE="TOKAN_TABLE";
    private final String ID_TOKAN = "ID";
    static final String TOKAN_LOGIN_JSON="TOKAN_LOGIN_JSON";



    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTokanInfo(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //----------------------------- Create Table TokanInfo -----------------------------------------//
    public void createTokanInfo(SQLiteDatabase db)
    {
        try {
            String sql = "CREATE TABLE " + TOKAN_TABLE + " ( "
                    + ID_TOKAN + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + TOKAN_LOGIN_JSON + " TEXT );";
            db.execSQL(sql);
            System.out.println("database created successfully");
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
    //====================================METHOD FOR INSERTING DATA INTO TOKAN TABLE =======================
    public void insertTokanJson(String tokanJson) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
        //    String deleteQuery = "DELETE FROM " + TBL_CANE_TYPE_LIST + ";";
         //   db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(TOKAN_LOGIN_JSON, tokanJson);
            db.insert(TOKAN_TABLE, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    public String getTokanJson()
    {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TOKAN_TABLE +"DESC LIMIT 1"+ ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception ex) {
            value = "Database is not found";
//            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return value;
    }
}
