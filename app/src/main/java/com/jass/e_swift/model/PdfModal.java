package com.jass.e_swift.model;

public class PdfModal {
        String pdf_id,pdf_name,pdf_thambnail,pdf_description;

    public String getPdf_id() {
        return pdf_id;
    }

    public void setPdf_id(String pdf_id) {
        this.pdf_id = pdf_id;
    }

    public String getPdf_name() {
        return pdf_name;
    }

    public void setPdf_name(String pdf_name) {
        this.pdf_name = pdf_name;
    }

    public String getPdf_thambnail() {
        return pdf_thambnail;
    }

    public void setPdf_thambnail(String pdf_thambnail) {
        this.pdf_thambnail = pdf_thambnail;
    }

    public String getPdf_description() {
        return pdf_description;
    }

    public void setPdf_description(String pdf_description) {
        this.pdf_description = pdf_description;
    }
}
