package com.jass.e_swift.model;

public class AttendanceModal {
   public String attendance_dt,status;

    public String getAttendance_dt() {
        return attendance_dt;
    }

    public void setAttendance_dt(String attendance_dt) {
        this.attendance_dt = attendance_dt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
