package com.jass.e_swift.model;

public class QuestionAnswerModel
{
    private String option1;
    private String option2;
    private String option3;
    private String option4;

    private String correct_ans;

    private String question;



    private String id;
    private String subject;
    private String selectedAnswer;
    private String isCorrect ;

    public String getIsSkipped() {
        return isSkipped;
    }

    public void setIsSkipped(String isSkipped) {
        this.isSkipped = isSkipped;
    }

    private String isSkipped;

    public String isCorrect() {
        return isCorrect;
    }

    public void setCorrect(String correct) {
        isCorrect = correct;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getCorrect_ans() {
        return correct_ans;
    }

    public void setCorrect_ans(String correct_ans) {
        this.correct_ans = correct_ans;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public String getSelectedAnswer ()
    {
        return selectedAnswer;
    }

    public void setSelectedAnswer (String selectedAnswer)
    {
        this.selectedAnswer = selectedAnswer;
    }
}

