package com.jass.e_swift.model;

public class LoginModal {
    String id,mob,instituteid,year,end_month,tokan,session_flag,return_val,monthValidation,yearValidation,userValidation,start_month;

    public String getStart_month() {
        return start_month;
    }

    public void setStart_month(String start_month) {
        this.start_month = start_month;
    }

    public String getMonthValidation() {

        return monthValidation;
    }

    public void setMonthValidation(String monthValidation) {
        this.monthValidation = monthValidation;
    }

    public String getYearValidation() {
        return yearValidation;
    }

    public void setYearValidation(String yearValidation) {
        this.yearValidation = yearValidation;
    }

    public String getUserValidation() {
        return userValidation;
    }

    public void setUserValidation(String userValidation) {
        this.userValidation = userValidation;
    }


    public String getReturn_val() {

        return return_val;
    }

    public void setReturn_val(String return_val) {
        this.return_val = return_val;
    }

    public String getSession_flag() {
        return session_flag;
    }

    public void setSession_flag(String session_flag) {
        this.session_flag = session_flag;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getInstituteid() {
        return instituteid;
    }

    public void setInstituteid(String instituteid) {
        this.instituteid = instituteid;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEnd_month() {
        return end_month;
    }

    public void setEnd_month(String end_month) {
        this.end_month = end_month;
    }

    public String getTokan() {
        return tokan;
    }

    public void setTokan(String tokan) {
        this.tokan = tokan;
    }
}
