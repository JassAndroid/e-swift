package com.jass.e_swift.model;

public class VideoModal {
    String video_id,video_name,video_thambnail,video_description;

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_thambnail() {
        return video_thambnail;
    }

    public void setVideo_thambnail(String video_thambnail) {
        this.video_thambnail = video_thambnail;
    }

    public String getVideo_description() {
        return video_description;
    }

    public void setVideo_description(String video_description) {
        this.video_description = video_description;
    }
}
