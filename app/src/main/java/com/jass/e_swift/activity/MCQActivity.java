package com.jass.e_swift.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.jass.e_swift.Dialog.ExamStatusDialog;
import com.jass.e_swift.R;
import com.jass.e_swift.Utility.APIRestManager;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.Utility.Preferences;
import com.jass.e_swift.model.MCQSSubjectModel;
import com.jass.e_swift.model.McqQuestionAnsModal;
import com.jass.e_swift.model.QuestionAnswerModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class MCQActivity extends AppCompatActivity {
    TextView txt_question, btnPrevious, btnNext, timer, toolbarTitle;
    LinearLayout optionOne, optionTwo, optionThree, optionFour;
    TextView txt_optionOne, txt_optionTwo, txt_optionThree, txt_optionFour;
    ImageView img_optionOne, img_optionTwo, img_optionThree, img_optionFour;

    ArrayList<McqQuestionAnsModal> questionList = new ArrayList<>();
    McqQuestionAnsModal selectedModal;
    McqQuestionAnsModal modal;
    int counter = 0, correctQues = 0, wrongQues = 0, skipQues = 0;
    Button btnExamStatus;
    TextView btnEndExam;
    TextView txtQuestionResult;
    TextView txtCurrentQuesNo;
    TextView txtSkip;
    Context context;
    LinearLayout linear_option1;
    ImageView imgBack;

    int count = 0;

    CountDownTimer cTimer;
    ProgressDialog progressDialog1;
    ArrayList<QuestionAnswerModel> getQuestion;
    private android.support.v4.app.FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq);
        init();


        buttonListeners();


        if (AppConstant.isTest) {
            setCountDown();
            getMcqList();
         //   setJsonDataToView();
            //  btnEndExam.setVisibility(View.GONE);


        } else {
            getMcqList();
         //  setJsonDataToView();
            timer.setVisibility(View.GONE);
        }


    }
    void getMcqList()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ins_id", "1");
            jsonObject.put("sub_id", "1");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIRestManager().postJSONArrayAPI(AppConstant.GET_MCQ_LIST, jsonObject, McqQuestionAnsModal.class, context, new APIRestManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObject) {

                questionList = (ArrayList<McqQuestionAnsModal>) resultObject;
                setJsonDataToView();
             //   mcqsSubjectAdapter.updateAdapter(list,key);

            }

            @Override
            public void onError(String msg) {
                //   loader.dismissLoader();
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });

    }
    void setCountDown() {
        cTimer = new CountDownTimer(300000 * 6, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long min = (millisUntilFinished / 1000) / 60;
                long sec = (millisUntilFinished / 1000) % 60;
                timer.setText("Remaining Time: " + min + " : " + sec);

            }

            @Override
            public void onFinish() {
                cTimer.cancel();

                showEndExamAlertDialog("Exam Ended", "Time limit is over. Press Okay to see Result", false);
            }
        }.start();

    }

    public void showEndExamAlertDialog(String title, String message, boolean isFinished) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MCQActivity.this);
        alertDialog.setTitle(title);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (AppConstant.isTest) {
                    cTimer.cancel();
                }
                // if (!AppConstant.isTest) {
                // AppConstants.queAnsModelArray = questionList;
                new ExamStatusDialog(context, questionList).show();
                //       //  }
                //  finish();
            }
        });

        if (!isFinished) {
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });
        }

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    public void init() {
        Preferences.appContext = this;
        context = this;
        selectedModal = new McqQuestionAnsModal();
        timer = findViewById(R.id.timer);
        imgBack = (ImageView) findViewById(R.id.img_back);
        txt_question = (TextView) findViewById(R.id.txt_question);
        txtSkip = (TextView) findViewById(R.id.btn_skip);
        linear_option1 = (LinearLayout) findViewById(R.id.linear_option1);
        optionOne = (LinearLayout) findViewById(R.id.option_1);
        optionTwo = (LinearLayout) findViewById(R.id.option_2);
        optionThree = (LinearLayout) findViewById(R.id.option_3);
        optionFour = (LinearLayout) findViewById(R.id.option_4);

        txt_optionOne = (TextView) findViewById(R.id.txt_option_1);
        txt_optionTwo = (TextView) findViewById(R.id.txt_option_2);
        txt_optionThree = (TextView) findViewById(R.id.txt_option_3);
        txt_optionFour = (TextView) findViewById(R.id.txt_option_4);

        txtQuestionResult = (TextView) findViewById(R.id.txtUserSelectionResult);
        txtCurrentQuesNo = (TextView) findViewById(R.id.txtCurrentQuesNo);


        img_optionOne = (ImageView) findViewById(R.id.radio_option_1);
        img_optionTwo = (ImageView) findViewById(R.id.radio_option_2);
        img_optionThree = (ImageView) findViewById(R.id.radio_option_3);
        img_optionFour = (ImageView) findViewById(R.id.radio_option_4);

        btnNext = (TextView) findViewById(R.id.btn_next);
        btnPrevious = (TextView) findViewById(R.id.btn_previous);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        //   timer = (TextView) findViewById(R.id.timer);

        btnExamStatus = (Button) findViewById(R.id.btn_exam_status);
        fragmentManager = getSupportFragmentManager();

        btnEndExam = (TextView) findViewById(R.id.txt_end_exam);

        toolbarTitle.setText(AppConstant.selectedSubject);

        if (!AppConstant.isDemo) {
            txtQuestionResult.setVisibility(View.GONE);
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("mcq.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    ArrayList<McqQuestionAnsModal> parseJsonData() {
        try {
            McqQuestionAnsModal modal;
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray countries = obj.getJSONArray("response");
            for (int i = 0; i < countries.length(); i++) {
                modal = new McqQuestionAnsModal();

                JSONObject jsonObject = countries.getJSONObject(i);
                modal.setMcq_ques(jsonObject.getString("question"));
                modal.setOption_1(jsonObject.getString("option1"));
                modal.setOption_2(jsonObject.getString("option2"));
                modal.setOption_3(jsonObject.getString("option3"));
                modal.setOption_4(jsonObject.getString("option4"));
                modal.setAnswer(jsonObject.getString("correct_ans"));

                questionList.add(modal);
                System.out.println("question" + questionList.get(i).getMcq_ques());
                System.out.println("option1" + questionList.get(i).getOption_1());
                System.out.println("option2" + questionList.get(i).getOption_2());
                System.out.println("option3" + questionList.get(i).getOption_3());
                System.out.println("option4" + questionList.get(i).getOption_4());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return questionList;
    }

    void alartDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure, You wanted to End Practice");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //     Toast.makeText(MCQActivity.this,"You clicked yes button",Toast.LENGTH_LONG).show();
                        new ExamStatusDialog(context, questionList).show();
//finish();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //   finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void buttonListeners() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter != questionList.size() - 1)
                    resetRadioButtons(null, null, null);
                if (counter < questionList.size()-1) {
                    counter++;
                  //  skipQues++;
                   // questionList.get(counter).setIsSkipped("true");

                    selectedModal = questionList.get(counter);
                    txt_optionOne.setText(selectedModal.getOption_1());
                    txt_optionTwo.setText(selectedModal.getOption_2());
                    txt_optionThree.setText(selectedModal.getOption_3());
                    txt_optionFour.setText(selectedModal.getOption_4());

                    txt_question.setText("" + (counter + 1) + ". " + selectedModal.getMcq_ques());


                    //Save last question index so will remeber user choice
               //     Preferences.setLastSolvedQuestion(AppConstant.selectedSubjectIndex,counter);


                    txtCurrentQuesNo.setText((counter + 1) + " Of " + questionList.size());
                     questionList.remove(counter);
                }
            }

        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (counter != questionList.size() - 1)
                    resetRadioButtons(null, null, null);
                if (counter < questionList.size() - 1) {
                    //p   if(selectedModal!=null) {
                    //p if (selectedModal.getSelectedAnswer() != null) {
                    counter++;
                    selectedModal = questionList.get(counter);
                    setQuestionAndAnswer(selectedModal);
                    //p }
                    //p  else
                    //p  {
                    //p   Toast.makeText(context,"Please select Answer",Toast.LENGTH_SHORT).show();

                    //p  }
                    //p  }
                    //p   else
                    //p   {
                    //p     Toast.makeText(context,"Please select Answer",Toast.LENGTH_SHORT).show();
                    //p    }
                } else {
                    //showAlert("Last Question");
                }

            }
        });

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (counter != 0)
                    resetRadioButtons(null, null, null);
                if (counter > 0) {
                    //p  if(selectedModal!=null) {
                    //p    if (selectedModal.getSelectedAnswer() != null) {

                    counter--;
                    selectedModal = questionList.get(counter);
                    setQuestionAndAnswer(selectedModal);
                    //p  }
                    //p   else
                    //p{
                    //p    Toast.makeText(context,"Please select Answer",Toast.LENGTH_SHORT).show();

                    //p  }

                    //p     }
                    //p   else
                    //p     {
                    //p  Toast.makeText(context,"Please select Answer",Toast.LENGTH_SHORT).show();
                    //p   }

                } else {
                    // showAlert("First Question");
                }
            }
        });


        btnEndExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  if(AppConstant.isTest)
                {
                    int givenAns = 0;

                    for (int i = 0; i < questionList.size(); i++) {
                        QuestionAnswerModel model = questionList.get(i);

                        String selAns = model.getSelectedAnswer();
                        if (selAns != null && !selAns.isEmpty()) {
                            givenAns++;
                        }
                    }

                    if (givenAns != questionList.size()) {
//                    Toast.makeText(ExamPanalActivity.this, "Solving all the questions is mandatory", Toast.LENGTH_LONG).show();
                        Snackbar.make(view, "Solving all the questions is mandatory", Snackbar.LENGTH_LONG).show();
                    } else {
                        showEndExamAlertDialog("End Exam", "Are you sure want to End Exam?", false);
                    }


                }*/
                //p   else
                {
                    showEndExamAlertDialog("End Practice", "Are you sure want to End Practice ?", false);

                }

            }
            //alartDialog();


        });

        optionOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetRadioButtons(img_optionOne, txt_optionOne, linear_option1);
            }
        });

        optionTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetRadioButtons(img_optionTwo, txt_optionTwo, optionTwo);
            }
        });

        optionThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetRadioButtons(img_optionThree, txt_optionThree, optionThree);

            }
        });

        optionFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetRadioButtons(img_optionFour, txt_optionFour, optionFour);
            }
        });
    }


    public void setJsonDataToView() {
        //questionList =new ArrayList<>();

       // questionList = parseJsonData();
        modal = new McqQuestionAnsModal();
        modal = questionList.get(counter);
        System.out.println("question counter=" + counter + "");
        System.out.println("question counter=" + modal.getMcq_ques());
        setQuestionAndAnswer(modal);

    }

    public void setQuestionAndAnswer(McqQuestionAnsModal questionAndAnswer) {


        txt_optionOne.setText(questionAndAnswer.getOption_1());
        txt_optionTwo.setText(questionAndAnswer.getOption_2());
        txt_optionThree.setText(questionAndAnswer.getOption_3());
        txt_optionFour.setText(questionAndAnswer.getOption_4());

        txt_question.setText("" + (counter + 1) + ". " + questionAndAnswer.getMcq_ques());


        //Save last question index so will remeber user choice
        Preferences.setLastSolvedQuestion(AppConstant.selectedSubjectIndex,counter);


        txtCurrentQuesNo.setText((counter + 1) + " Of " + questionList.size());

        if (questionAndAnswer.getSelectedAnswer() == null) {
            resetRadioButtons(null, null, null);
        } else {
            getSelectedRadioButton(questionAndAnswer);
        }


        if (counter == 0) {
            btnPrevious.setAlpha(0.5f);
            btnPrevious.setEnabled(false);
            btnNext.setAlpha(1f);
            btnNext.setEnabled(true);
        } else if (counter == questionList.size() - 1) {
            btnNext.setAlpha(0.5f);
            btnNext.setEnabled(false);
            btnPrevious.setAlpha(1f);
            btnPrevious.setEnabled(true);
        } else {
            btnNext.setEnabled(true);
            btnPrevious.setEnabled(true);
            btnNext.setAlpha(1f);
            btnPrevious.setAlpha(1f);
        }

    }

    private void getSelectedRadioButton(McqQuestionAnsModal model) {
        if (model.getSelectedAnswer().equals(txt_optionOne.getText().toString().replace("A) ", "").trim())) {
            img_optionOne.setBackgroundResource(R.drawable.checkbox_selected);
        } else if (model.getSelectedAnswer().equals(txt_optionTwo.getText().toString().replace("B) ", "").trim())) {
            img_optionTwo.setBackgroundResource(R.drawable.checkbox_selected);
        } else if (model.getSelectedAnswer().equals(txt_optionThree.getText().toString().replace("C) ", "").trim())) {
            img_optionThree.setBackgroundResource(R.drawable.checkbox_selected);
        } else if (model.getSelectedAnswer().equals(txt_optionFour.getText().toString().replace("D) ", "").trim())) {
            img_optionFour.setBackgroundResource(R.drawable.checkbox_selected);
        }
    }

    private void resetRadioButtons(ImageView radio, TextView txtView, LinearLayout opt) {
        img_optionOne.setBackgroundResource(R.drawable.checkbox_normal);

        img_optionTwo.setBackgroundResource(R.drawable.checkbox_normal);

        img_optionThree.setBackgroundResource(R.drawable.checkbox_normal);

        img_optionFour.setBackgroundResource(R.drawable.checkbox_normal);

        linear_option1.setBackgroundColor(Color.parseColor("#ffffff"));
        optionTwo.setBackgroundColor(Color.parseColor("#ffffff"));
        optionThree.setBackgroundColor(Color.parseColor("#ffffff"));
        optionFour.setBackgroundColor(Color.parseColor("#ffffff"));
        txtQuestionResult.setText("");

        if (radio != null) {
            radio.setBackgroundResource(R.drawable.checkbox_selected);

            String selectedText = txtView.getText().toString();
            selectedText = selectedText.replace("A) ", "").trim();
            selectedText = selectedText.replace("B) ", "").trim();
            selectedText = selectedText.replace("C) ", "").trim();
            selectedText = selectedText.replace("D) ", "").trim();

            selectedModal = questionList.get(counter);
            selectedModal.setSelectedAnswer(selectedText);
            questionList.remove(counter);
            questionList.add(counter, selectedModal);

            String correctAns = selectedModal.getAnswer().trim();
            if (AppConstant.isTest) {
                if (selectedText.equalsIgnoreCase(correctAns)) {
//                correctQues++;
                    //   txtQuestionResult.setText("Correct");
                    //   opt.setBackgroundColor(Color.parseColor("#0cd151"));
                    questionList.get(counter).setIsCorrect("true");
                    //  txtQuestionResult.setTextColor(getResources().getColor(R.color.correctAnswer));
                } else {
//                wrongQues++;
                    //  txtQuestionResult.setText("Wrong Answer");
                    questionList.get(counter).setIsCorrect("false");
                    //    opt.setBackgroundColor(Color.parseColor("#ec331e"));
                    //txtQuestionResult.setTextColor(getResources().getColor(R.color.wrongAnswer));
                }
            } else {
                if (selectedText.equalsIgnoreCase(correctAns)) {
//                correctQues++;
                    //   txtQuestionResult.setText("Correct");
                    opt.setBackgroundColor(Color.parseColor("#0cd151"));
                    questionList.get(counter).setIsCorrect("true");
                    //  txtQuestionResult.setTextColor(getResources().getColor(R.color.correctAnswer));
                } else {
//                wrongQues++;
                    //  txtQuestionResult.setText("Wrong Answer");
                    questionList.get(counter).setIsCorrect("false");
                    opt.setBackgroundColor(Color.parseColor("#ec331e"));
                    //txtQuestionResult.setTextColor(getResources().getColor(R.color.wrongAnswer));
                }
            }
        }


    }


}
