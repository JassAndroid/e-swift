package com.jass.e_swift.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jass.e_swift.R;

public class CustomToolbar extends AppCompatActivity {
    Context context = this;
    ImageView imgHome;
    TextView txtTitle;
    ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_toolbar);
    }
    protected void setUpToolBar(String title)
    {
        txtTitle = (TextView) findViewById(R.id.txt_toolbar);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgHome = (ImageView) findViewById(R.id.img_home);

        try {
            txtTitle.setText(title);

            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            imgHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Search for start activity with intent using CLEAR_FLAG
                    Intent i = new Intent(getApplicationContext(),DashBoardActivity.class);
                    i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK |i.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i); // Launch the HomescreenActivity
                    finish();         // Close down the SettingsActivity

                }
            });
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }

    }
}
