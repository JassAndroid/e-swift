package com.jass.e_swift.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.jass.e_swift.R;

public class VideoTutorialActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String API_KEY = "AIzaSyAFmE47u8whCsyXdENKIpZjkLV0DlBT_cc";
    public static final String YOUTUBE_VIDEO_CODE = "QjmTTWkHEag";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer mYouTubePlayer;
    ImageView imgBack;
    TextView text;
    private EditText edtSeek;
    private Button btnSeek;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_tutorial);
/*        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

//        text = findViewById(R.id.text);
//        text.setText("Hello");


        YouTubePlayerView youTubeView = findViewById(R.id.youtube_view);
        youTubeView.initialize(API_KEY, this);

        init();
    }

    private void init() {
        edtSeek = findViewById(R.id.edt_seek);
        btnSeek = findViewById(R.id.btn_seek);
        imgBack = (ImageView) findViewById(R.id.img_back);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtSeek.getText().toString().length() > 0) {
                    int seekPos = Integer.parseInt(edtSeek.getText().toString());
                    mYouTubePlayer.seekToMillis(seekPos);

                    mYouTubePlayer.getCurrentTimeMillis();

                } else {
                    Toast.makeText(VideoTutorialActivity.this, "Enter seek position", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer.loadVideo(YOUTUBE_VIDEO_CODE);

            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            mYouTubePlayer = youTubePlayer;
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {

        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format("Error playing video: %s", errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            getYouTubePlayerProvider().initialize(API_KEY, this);
        }
    }
}

