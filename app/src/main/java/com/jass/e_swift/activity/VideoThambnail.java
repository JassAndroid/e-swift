package com.jass.e_swift.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jass.e_swift.R;
import com.jass.e_swift.Utility.DatabaseConnection;
import com.jass.e_swift.adapter.videoAdapter;
import com.jass.e_swift.model.VideoModal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VideoThambnail extends CustomToolbar {
    RecyclerView recyclerView;
    videoAdapter adapter;
    ArrayList<VideoModal> videoModalArrayList;
    Context context;
    VideoModal videoModal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_thambnail);
        setUpToolBar("Video Thambnail");
        init();
        getVideoUrl();
    }
    void init()
    {
        recyclerView=findViewById(R.id.recycle_view_video);
        context=this;
        videoModalArrayList=new ArrayList<>();
        adapter=new videoAdapter(videoModalArrayList,context);
        setupRecyclerView();
    }
    void setupRecyclerView()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
    }
    void getVideoUrl()
    {
        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection dbCon = DatabaseConnection.getSqlConnection();

        //  Connection dbCon= DatabaseConnection.getSqlConnection();
        System.out.println("subject="+dbCon);
        try {
            statement = dbCon.prepareStatement("select video_id,video_name,video_thambnail,video_description from mobile_dev.VIDEO where subject_id=1");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //  statement.setString(1,ID);
        try {
            rs = statement.executeQuery();
            while(rs.next()) {
                System.out.println("ID="+String.valueOf(rs.getString("video_id")));
                videoModal=new VideoModal();
                videoModal.setVideo_id(String.valueOf(rs.getString("video_id")));
                videoModal.setVideo_name(String.valueOf(rs.getString("video_name")));
                videoModal.setVideo_thambnail(String.valueOf(rs.getString("video_thambnail")));
                videoModal.setVideo_description(String.valueOf(rs.getString("video_description")));
                videoModalArrayList.add(videoModal);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(int i=0;i<4;i++)
        {
             videoModalArrayList.add(videoModalArrayList.get(i));
        }
        adapter.updateAdapter(videoModalArrayList,context);

        try {
            dbCon.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //  dev.close();

    }

}
