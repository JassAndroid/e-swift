package com.jass.e_swift.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jass.e_swift.Fragment.MarksObtainedFragment;
import com.jass.e_swift.Fragment.StudentAttendanceFragment;
import com.jass.e_swift.R;
import com.jass.e_swift.adapter.ViewPagerAdapter;

public class StudentCurriculumActivity extends CustomToolbar {
    ViewPager simpleViewPager;
    TabLayout tabLayout;
    ViewPagerAdapter mAdapter;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_curriculum);
        setUpToolBar("Student Curriculum ");
        init();
    }

    public void init(){

        context =this;
       // setUpToolBar(getString(R.string.report_menu));


        simpleViewPager = findViewById(R.id.simpleViewPager);
        tabLayout = findViewById(R.id.tab_layout);

      //  attendance = new StudentAttendanceFragment("Attendance");
      //  marks_obtained = new StudentAttendanceFragment("Marks Obtained");

        setupViewPager();
        }


    private void setupViewPager() {
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        StudentAttendanceFragment attendanceFragment = new StudentAttendanceFragment();
        mAdapter.addFragment(attendanceFragment, "Attendance",R.mipmap.ic_attendance_icon);
        MarksObtainedFragment marks_obtained = new MarksObtainedFragment();
        mAdapter.addFragment(marks_obtained,"Marks Obtained",R.mipmap.ic_marks_obtained_icon );
        setViewPageAdapter();

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int index = tab.getPosition();
              //  selectCustomTab(tab,index);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setUpCustomTab();
    }

    private void selectCustomTab(TabLayout.Tab tab , int position){
        View view = tab.getCustomView();
        tab.setCustomView(view);
    }


    private void setViewPageAdapter() {
        simpleViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(simpleViewPager);
    }



    @SuppressLint("RestrictedApi")
    private void setUpCustomTab() {
        for (int i = 0; i < mAdapter.getCount(); i++) {

            View view = LayoutInflater.from(context).inflate(R.layout.custom_tab_bar, null);
            TextView txtTitle = view.findViewById(R.id.txt_tab_name);
            ImageView imgTab = view.findViewById(R.id.img_tab);

//set title and icon
            txtTitle.setText(mAdapter.getPageTitle(i));
            imgTab.setImageDrawable(ContextCompat.getDrawable(context, mAdapter.getPageDrawable(i)));
            if (i == 0) {
                txtTitle.setText("Attendance");
            }
            tabLayout.getTabAt(i).setCustomView(view);
        }
       // selectCustomTab(tabLayout.getTabAt(0));
    }
}
