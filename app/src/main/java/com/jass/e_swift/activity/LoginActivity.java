package com.jass.e_swift.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jass.e_swift.DbHelpar.SQLHelper;
import com.jass.e_swift.R;
import com.jass.e_swift.Utility.APIRestManager;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.Utility.CustomLoader;
import com.jass.e_swift.Utility.DatabaseConnection;
import com.jass.e_swift.Utility.Preferences;
import com.jass.e_swift.adapter.DashboardAdapter;
import com.jass.e_swift.model.LoginModal;
import com.jass.e_swift.model.MCQSSubjectModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginActivity extends AppCompatActivity {
    Context context;

    EditText edt_login_institude_code,edt_login_reg_number,edt_login_password;
    Button login_button;
    LoginModal loginModal;
    CustomLoader loader;
    private SQLHelper sqLiteHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    public void init(){
        context = this;
        loginModal=new LoginModal();
        loader= new CustomLoader(LoginActivity.this);
        edt_login_institude_code = findViewById(R.id.edt_login_institude_code);
        edt_login_reg_number  = findViewById(R.id.edt_login_reg_number);
        edt_login_password = findViewById(R.id.edt_login_password);
        login_button = findViewById(R.id.login_button);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loader.showLoader();
                if(isValidate()){
                    //loginValidation();
                    loginApi();
                }
            }
        });

    }
void loginApi()
{
    System.out.println("i m inside api login");
    JSONObject param = new JSONObject();
    try {
        param.put("instituteid", edt_login_institude_code.getText().toString());
        param.put("mob",edt_login_reg_number.getText().toString());
        param.put("passw", edt_login_password.getText().toString());
    } catch (JSONException e) {
        e.printStackTrace();
    }
    System.out.println("i m inside api login");
  //  pdLoading.dismiss();
    AppConstant.isLoginActivity=true;
    new APIRestManager().postObjectAPI(context,AppConstant.USER_LOGIN_API, LoginModal.class,param, new APIRestManager.APIManagerInterface() {
        @Override
        public void onSuccess(Object resultObj) {
            loginModal = (LoginModal) resultObj;

            if(loginModal!=null)
            {
                loader.dismissLoader();
                if(loginModal.getUserValidation().equalsIgnoreCase("validUser"))
                {
                    if(loginModal.getYearValidation().equalsIgnoreCase("validYear"))
                    {
                        if(loginModal.getMonthValidation().equalsIgnoreCase("validMonth"))
                        {
                            sqLiteHelper = new SQLHelper(context);
                            System.out.println("jsonstring="+AppConstant.jsonString);
                            sqLiteHelper.insertTokanJson(AppConstant.jsonString);
                            Preferences.setFirstTimeLoginStatus(true);
                            startActivity(new Intent(context,DashBoardActivity.class));
                            finish();
                        }
                        else if(loginModal.getMonthValidation().equalsIgnoreCase("InvalidMonth"))
                        {
                            Toast.makeText(context,"Session Expire",Toast.LENGTH_LONG).show();
                        }
                    }
                    else if(loginModal.getYearValidation().equalsIgnoreCase("InvalidYear"))
                    {
                        Toast.makeText(context,"Your Session Is Expire",Toast.LENGTH_LONG).show();
                    }

                }
                else
                {
                    Toast.makeText(context,"Please Enter Right User Name And Password",Toast.LENGTH_LONG).show();

                }
              //  Toast.makeText(context,"Login Successfull",Toast.LENGTH_LONG).show();

            }
        }

        @Override
        public void onError(String strError) {
          //  pdLoading.dismiss();
            loader.dismissLoader();
            Toast.makeText(context, strError, Toast.LENGTH_SHORT).show();
        }
    });
}
    boolean isValidate(){
        if (edt_login_institude_code.getText().length() == 0) {
            Toast.makeText(context, "Please Enter your Institute Code", Toast.LENGTH_SHORT).show();
            return false;
        }else if (edt_login_reg_number.getText().length()== 0) {
            Toast.makeText(context, "Please Enter your Registration Number", Toast.LENGTH_SHORT).show();
            return false;
        }else if(edt_login_password.getText().length() == 0) {
            Toast.makeText(context, "Please Enter your Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
