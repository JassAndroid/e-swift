package com.jass.e_swift.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jass.e_swift.R;
import com.jass.e_swift.Utility.DatabaseConnection;
import com.jass.e_swift.adapter.PdfThambnailAdapter;
import com.jass.e_swift.model.PdfModal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EbookThambnailActivity extends CustomToolbar {
    RecyclerView recyclerView;
    Context context;
    ArrayList<PdfModal> pdfThambnailModalArrayList;
    PdfThambnailAdapter adapter;
    PdfModal pdfModal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook_thambnail);
        setUpToolBar("Pdf Thambnail");
        init();
        getPdfThambnail();
    }
    void init()
    {
        recyclerView=findViewById(R.id.recycle_view_video);
        context=this;
        pdfThambnailModalArrayList=new ArrayList<>();
        adapter=new PdfThambnailAdapter(pdfThambnailModalArrayList,context);
        setupRecyclerView();
    }
    void setupRecyclerView()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
    }
    void getPdfThambnail()
    {
        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection dbCon = DatabaseConnection.getSqlConnection();

        //  Connection dbCon= DatabaseConnection.getSqlConnection();
     //   System.out.println("subject="+dbCon);
        try {
            statement = dbCon.prepareStatement("select pdf_id,pdf_name,pdf_thambnail from mobile_dev.PDF where subject_id=1");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //  statement.setString(1,ID);
        try {
            rs = statement.executeQuery();
            while(rs.next()) {
              //  System.out.println("ID="+String.valueOf(rs.getString("video_id")));
                pdfModal=new PdfModal();
                pdfModal.setPdf_id(String.valueOf(rs.getString("pdf_id")));
                pdfModal.setPdf_name(String.valueOf(rs.getString("pdf_name")));
                pdfModal.setPdf_thambnail(String.valueOf(rs.getString("pdf_thambnail")));
                pdfThambnailModalArrayList.add(pdfModal);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(int i=0;i<3;i++)
        {
            pdfThambnailModalArrayList.add(pdfThambnailModalArrayList.get(i));
        }
        adapter.updateAdapter(pdfThambnailModalArrayList,context);

        try {
            dbCon.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
