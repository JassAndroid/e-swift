package com.jass.e_swift.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.jass.e_swift.R;
import com.jass.e_swift.Utility.Preferences;


public class SplashActivity extends AppCompatActivity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        Preferences.appContext=context;



        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                    System.out.println("first time login status="+Preferences.getFirstTimeLoginStatus());
                    if(Preferences.getFirstTimeLoginStatus())
                    {
                        System.out.println("first time login status="+Preferences.getFirstTimeLoginStatus());
                        startActivity(new Intent(SplashActivity.this,DashBoardActivity.class));
                        finish();
                    }
                    else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
