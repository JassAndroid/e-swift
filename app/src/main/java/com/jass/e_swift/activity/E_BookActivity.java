package com.jass.e_swift.activity;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.jass.e_swift.R;


public class E_BookActivity extends CustomToolbar{


    WebView webview;
    ProgressBar progressbar;
    String pdf_name;
    public static final String PDF_URL="http://eswift.in/BookPdf/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e__book);
        init();
        setUpToolBar(pdf_name);


    }
    void init()
    {
        Intent intent=getIntent();
        pdf_name=intent.getStringExtra("pdf_name");
        webview = findViewById(R.id.webview);
        progressbar = findViewById(R.id.progressbar);
        getPdf();
    }
    void getPdf()
    {
        webview.getSettings().setJavaScriptEnabled(true);
      //  String filename ="https://www.tutorialspoint.com/android/android_tutorial.pdf";
        String filename=PDF_URL.concat(pdf_name);
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + filename);

        webview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                progressbar.setVisibility(View.GONE);
            }
        });

    }
}


