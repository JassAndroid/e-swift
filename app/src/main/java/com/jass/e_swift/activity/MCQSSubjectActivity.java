package com.jass.e_swift.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jass.e_swift.R;
import com.jass.e_swift.Utility.APIRestManager;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.Utility.DatabaseConnection;
import com.jass.e_swift.adapter.DashboardAdapter;
import com.jass.e_swift.adapter.MCQSSubjectAdapter;
import com.jass.e_swift.model.CuricularModel;
import com.jass.e_swift.model.MCQSSubjectModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MCQSSubjectActivity extends CustomToolbar {

    Context context;
    TextView txt_mcqs_subject_name;
    ImageView img_mcqs_subject;
    ArrayList<MCQSSubjectModel> list;
    MCQSSubjectModel mcqsSubjectModel;
    MCQSSubjectAdapter mcqsSubjectAdapter;
    RecyclerView mRecycleView;
    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcqssubject);
        setUpToolBar("Subject List");
        // makedataset();


        init();
        setUpRecycleView();
        subjectListApi();
    }

    void subjectListApi() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subject_id", "1");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIRestManager().postJSONArrayAPI(AppConstant.GET_SUBJECT_COURSE_CONTENT, jsonObject, MCQSSubjectModel.class, context, new APIRestManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObject) {

                list = (ArrayList<MCQSSubjectModel>) resultObject;
                mcqsSubjectAdapter.updateAdapter(list,key);

            }

            @Override
            public void onError(String msg) {
                //   loader.dismissLoader();
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });

    }


   /* void getSubjectList()
    {
        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection dbCon = DatabaseConnection.getSqlConnection();

      //  Connection dbCon= DatabaseConnection.getSqlConnection();
       System.out.println("subject="+dbCon);
        try {
            statement = dbCon.prepareStatement("select * from mobile_dev.SUB_CONTENT where subject_id=1");
        } catch (SQLException e) {
            e.printStackTrace();
        }
      //  statement.setString(1,ID);
        try {
            rs = statement.executeQuery();
            while(rs.next()) {
                System.out.println("ID="+String.valueOf(rs.getString("ID")));
                mcqsSubjectModel=new MCQSSubjectModel();
             mcqsSubjectModel.setID(String.valueOf(rs.getString("ID")));
               mcqsSubjectModel.setName(String.valueOf(rs.getString("content_name")));
               list.add(mcqsSubjectModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(int i=0;i<list.size();i++)
        {
            System.out.println("ID="+list.get(i).getID());
            System.out.println("content name="+list.get(i).getName());
        }
        mcqsSubjectAdapter.updateAdapter(list,key);

        try {
            dbCon.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //  dev.close();

        }
*/

    public void init(){
        context = this;
        list = new ArrayList<>();
        Intent intent=getIntent();
        key=intent.getStringExtra("key");
       mRecycleView = findViewById(R.id.recycle_view);
        mcqsSubjectAdapter = new MCQSSubjectAdapter(context,list,key);

        txt_mcqs_subject_name = findViewById(R.id.txt_macqs_subject_name);
      //  img_mcqs_subject = findViewById(R.id.img_mcqs_subject);

    }

  /*  private void makedataset(){

        context = this;

        MCQSSubjectModel model1 = new MCQSSubjectModel();
        model1.setName("Java");
        model1.setImage(R.mipmap.ic_student_curriculum);
        list.add(model1);


        MCQSSubjectModel model2 = new MCQSSubjectModel();
        model2.setName("C++");
        model2.setImage(R.mipmap.ic_mcqs);
        list.add(model2);


        MCQSSubjectModel model3 = new MCQSSubjectModel();
        model3.setName("DBMS");
        model3.setImage(R.mipmap.ic_video);
        list.add(model3);

        MCQSSubjectModel model4 = new MCQSSubjectModel();
        model4.setName("My SQL");
        model4.setImage(R.mipmap.ic_e_book);
        list.add(model4);


        MCQSSubjectModel model5 = new MCQSSubjectModel();
        model5.setName("C");
        model5.setImage(R.mipmap.ic_e_book);
        list.add(model5);


        MCQSSubjectModel model6 = new MCQSSubjectModel();
        model6.setName("Oracle");
        model6.setImage(R.mipmap.ic_e_book);
        list.add(model6);


        MCQSSubjectModel model7 = new MCQSSubjectModel();
        model7.setName("Android");
        model7.setImage(R.mipmap.ic_e_book);
        list.add(model7);


        MCQSSubjectModel model8 = new MCQSSubjectModel();
        model8.setName("Hadoop");
        model8.setImage(R.mipmap.ic_e_book);
        list.add(model8);

    }
*/
    private void setUpRecycleView(){


       mRecycleView.setLayoutManager(new GridLayoutManager(context,2));
      //  mRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mRecycleView.setAdapter(mcqsSubjectAdapter);
    }
}
