package com.jass.e_swift.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jass.e_swift.DbHelpar.SQLHelper;
import com.jass.e_swift.R;
import com.jass.e_swift.Utility.APIRestManager;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.Utility.Preferences;
import com.jass.e_swift.adapter.DashboardAdapter;
import com.jass.e_swift.model.BackgroundLoginCheck;
import com.jass.e_swift.model.CuricularModel;
import com.jass.e_swift.model.LoginModal;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DashBoardActivity extends AppCompatActivity {
    Context context;
    TextView txt_dashboard;
    ImageView img_dashboard;
    ArrayList<CuricularModel> list;
    SQLHelper sqlHelper;
    String strLoginStatus, exeStatus;
    LoginModal loginModal;
    BackgroundLoginCheck  loginModalResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        makedataset();
        setUpRecycleView();
        init();
        // retriveJsonFromDb();
        if(Preferences.getFirstTimeLoginStatus()) {
            AsynkTaskRunner asynkTaskRunner = new AsynkTaskRunner();
            asynkTaskRunner.execute();
        }

    }

    public void init() {
        context = this;
        sqlHelper = new SQLHelper(context);
        loginModal = new LoginModal();
        loginModalResponse = new BackgroundLoginCheck();
        txt_dashboard = findViewById(R.id.txt_dashboard);
        img_dashboard = findViewById(R.id.img_dashboard);

    }

    int getTodaysMonth() {
        DateFormat dateFormat = new SimpleDateFormat("MM", Locale.ENGLISH);
        Date date = new Date();
        String str_todays_month = dateFormat.format(date);
        int todaysMonth = Integer.parseInt(str_todays_month);
        System.out.println("todays date=" + todaysMonth + "");
        return todaysMonth;

    }

    String retriveJsonFromDb() {
        String loginJson = sqlHelper.getTokanJson();
        System.out.println("Json String Stored in database" + loginJson);
        try {
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject(loginJson);
            loginModal = gson.fromJson(jsonObject.toString(), LoginModal.class);
            exeStatus = checkLoginValidationInBack();
         /*   int endYear = Integer.parseInt(loginModal.getYear());
            int endMonth = Integer.parseInt(loginModal.getEnd_month());
            System.out.println("db year" + endYear);
            if (endYear >= getCurrentYear()) {
                System.out.println("end year condition is valid");
                if (endMonth >= getTodaysMonth()) {
                    System.out.println("end month condition is valid");
                    exeStatus = checkLoginValidationInBack();
                } else {
                    //  Toast.makeText(context,"Session Expire",Toast.LENGTH_LONG).show();
                    //   startActivity(new Intent(context,LoginActivity.class));
                    exeStatus = "sessionExp";
                }

            } else {
                //    Toast.makeText(context,"Session Expire",Toast.LENGTH_LONG).show();
                // startActivity(new Intent(context,LoginActivity.class));
                exeStatus = "sessionExp";
            }*/

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return exeStatus;
    }

    int getCurrentYear() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = new Date();
        String str_current_year = dateFormat.format(date);
        int currentYear = Integer.parseInt(str_current_year);
        System.out.println("todays date=" + dateFormat.format(date));
        return currentYear;
    }

    private class AsynkTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String exe = retriveJsonFromDb();
            System.out.println("In Asynck Do in background" + exe);
            return exe;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    String checkLoginValidationInBack() {
        System.out.println("i m inside api login");
        JSONObject param = new JSONObject();

        try {
            param.put("instituteid", loginModal.getInstituteid());
            param.put("user_id", loginModal.getId());
            //  param.put("user_session",loginModal.getSession_flag());
            param.put("tokan", loginModal.getTokan());
            param.put("year",loginModal.getYear());
            param.put("start_month",loginModal.getStart_month());
            param.put("end_month",loginModal.getEnd_month());


            new APIRestManager().postObjectAPI(context, AppConstant.CHCK_LOGIN_IN_BACKGROUND, BackgroundLoginCheck.class, param, new APIRestManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    loginModalResponse = (BackgroundLoginCheck) resultObj;
                    strLoginStatus = loginModalResponse.getFlagVal();
                    if(loginModalResponse!=null)
                    {
                        if(loginModalResponse.getFlagVal()!=null) {
                            if (loginModalResponse.getFlagVal().equalsIgnoreCase("1")) {
                                Toast.makeText(context, "Background Login Valid", Toast.LENGTH_LONG).show();
                            } else if (loginModalResponse.getFlagVal().equalsIgnoreCase("2")) {
                                Toast.makeText(context, "Please ReLogin", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(context, LoginActivity.class));
                                finish();
                            } else if (loginModalResponse.getFlagVal().equalsIgnoreCase("3")) {
                                Toast.makeText(context, "Your Session Expire Cant login", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(context, LoginActivity.class));
                                finish();
                            } else if (loginModalResponse.getFlagVal().equalsIgnoreCase("4")) {
                                Toast.makeText(context, "Your Session Expire", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(context, LoginActivity.class));
                                finish();
                            } else if (loginModalResponse.getFlagVal().equalsIgnoreCase("5")) {
                                Toast.makeText(context,"App Is Already install on 1 Device",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(context, LoginActivity.class));
                                finish();
                            }
                        }

                    }
                }

                @Override
                public void onError(String strError) {
                    //  pdLoading.dismiss();
                    //   loader.dismissLoader();
                    strLoginStatus = "Error";
                    Toast.makeText(context, strError, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return strLoginStatus;
    }

    private void makedataset() {
        context = this;
        list = new ArrayList<>();

        CuricularModel model1 = new CuricularModel();
        model1.setName("Student Curriculum");
        model1.setImage(R.mipmap.ic_student_curriculum);
        list.add(model1);


        CuricularModel model2 = new CuricularModel();
        model2.setName("MCQ's");
        model2.setImage(R.mipmap.ic_mcqs);
        list.add(model2);


        CuricularModel model3 = new CuricularModel();
        model3.setName("Videos");
        model3.setImage(R.mipmap.ic_video);
        list.add(model3);

        CuricularModel model4 = new CuricularModel();
        model4.setName("E-Book");
        model4.setImage(R.mipmap.ic_e_book);
        list.add(model4);

    }

    private void setUpRecycleView() {
        RecyclerView mRecycleView = findViewById(R.id.recycle_view);
        DashboardAdapter dashboardAdapter = new DashboardAdapter(context, list);
        mRecycleView.setLayoutManager(new GridLayoutManager(context, 2));
        //    mRecycleView.setLayoutManager(new LinearLayoutManager(context));
        mRecycleView.setAdapter(dashboardAdapter);
    }
}
