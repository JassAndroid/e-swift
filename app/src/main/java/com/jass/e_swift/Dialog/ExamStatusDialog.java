package com.jass.e_swift.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.jass.e_swift.R;
import com.jass.e_swift.model.McqQuestionAnsModal;
import com.jass.e_swift.model.QuestionAnswerModel;

import java.util.ArrayList;

public class ExamStatusDialog extends Dialog {
    Button btnOk;
    Context context;
    TextView txt_total_qus_count, txt_correct_qus_count, txt_wrong_qus_count,txt_que_skip;
    int totalQue, correctQue, wrongQue, skippQue;
    ArrayList<McqQuestionAnsModal> list;

    public ExamStatusDialog(Context context, ArrayList<McqQuestionAnsModal> list) {
        super(context);
        this.context = context;
        this.list = list;
       // this.skippQue = skippQue;
        setCounters();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setDialogProperties();
    }

    private void setCounters() {
        this.totalQue = list.size();

        for (McqQuestionAnsModal questionAnswerModel : list) {
            if (questionAnswerModel.getIsCorrect() == null) {
                break;
            }
            if (questionAnswerModel.getIsCorrect().equalsIgnoreCase("true")) {
                correctQue++;
            } else if (questionAnswerModel.getIsCorrect().equalsIgnoreCase("false")) {
                wrongQue++;
            }

        }
    }

    private void setDialogProperties() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.exam_status_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
       /* WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.dimAmount = 0.8f;*/
        init();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.exit(0);
                ((Activity) context).finish();
                // ((Activity)context).finishAffinity();
            }
        });
    }

    void init() {

        btnOk = findViewById(R.id.btn_ok);
        txt_que_skip=findViewById(R.id.txt_que_skip);
        txt_total_qus_count = findViewById(R.id.txt_total_qus_count);
        txt_correct_qus_count = findViewById(R.id.txt_correct_qus_count);
        txt_wrong_qus_count = findViewById(R.id.txt_wrong_qus_count);

        txt_total_qus_count.setText(totalQue + "");
        txt_correct_qus_count.setText(correctQue + "");
        txt_que_skip.setText(skippQue+"");
        txt_wrong_qus_count.setText(wrongQue + "");
    }
}
