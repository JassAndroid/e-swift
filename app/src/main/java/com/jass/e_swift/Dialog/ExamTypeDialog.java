package com.jass.e_swift.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.jass.e_swift.R;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.Utility.Preferences;

public class ExamTypeDialog extends Dialog {
  public   RadioButton btnOnlineTest,btnPracticeTest;
  public RadioGroup rdo_group;
    public ExamTypeDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.exam_type_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        init();

    }
    private void setDialogProperties() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.exam_type_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
       /* WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.dimAmount = 0.8f;*/
        init();

    }
    void init(){


            btnOnlineTest = findViewById(R.id.rdo_online_test);
            btnPracticeTest = findViewById(R.id.rdo_practice_test);
        rdo_group=findViewById(R.id.rdo_group);
     /*       btnOnlineTest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstant.isTest=true;
                    dismiss();
                }
            });
            btnPracticeTest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   AppConstant.isTest=false;
                    dismiss();
                }
            });*/


    }
    interface SenDCallbackToActivityInterFase
    {
        void sendBackToCallingFunction();

    }
    public void setCallBack(SenDCallbackToActivityInterFase interfaceObj)
    {

    }
}
