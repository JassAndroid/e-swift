package com.jass.e_swift.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jass.e_swift.R;

public class MarksObtainedFragment  extends Fragment {
    public MarksObtainedFragment()
    {

    }
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.markes_obtained_fragment, container, false);
        init(view);
        return view;

    }
    void init(View view)
    {

    }
}
