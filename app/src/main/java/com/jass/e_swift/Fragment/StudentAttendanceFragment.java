package com.jass.e_swift.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.jass.e_swift.R;
import com.jass.e_swift.Utility.APIRestManager;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.Utility.MyEventDay;
import com.jass.e_swift.model.AttendanceModal;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StudentAttendanceFragment extends Fragment {
    Context context;
    String fragmentName;
    CalendarView mCalendarView;
    ArrayList<AttendanceModal> attendanceModalArrayList;

    MyEventDay myEventDay;
    private List<EventDay> mEventDays = new ArrayList<>();

    public StudentAttendanceFragment() {
// Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.students_attendance_fragment, container, false);
        init(view);
        return view;

    }
    void init(View view)
    {
        context=getContext();
        attendanceModalArrayList=new ArrayList<>();
         mCalendarView =view.findViewById(R.id.calendarView);
        getAttendance();
    }
    void calendarEvents()
    {
   //    CalendarView mCalendarView = (CalendarView) context.findViewById(R.id.calendarView);

              //  findViewById(R.id.calendarView);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        int i = 0;

        Date dateVal = null;

        Date newDate2 = null;


        for(int j=0;j<attendanceModalArrayList.size();j++)
        {
            try {
                newDate2=sdf.parse(attendanceModalArrayList.get(j).getAttendance_dt());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDate2);
            System.out.println(newDate2); //Tue Aug 31 10:20:56 SGT 1982
            mCalendarView.setDate(newDate2);


            if(attendanceModalArrayList.get(j).status.equalsIgnoreCase("p")) {
                myEventDay = new MyEventDay(calendar, R.drawable.tick);
            }
            else
            {
                myEventDay = new MyEventDay
                        (calendar, R.mipmap.ic_cancel);
            }
            mEventDays.add(myEventDay);
            mCalendarView.setEvents(mEventDays);
            //   mCalendarView.findFocus();
            //    mCalendarView.setDate(dateVal);
        }

    }
    void getAttendance()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject=null;

        new APIRestManager().postJSONArrayAPI(AppConstant.USER_LOGIN_API,jsonObject,AttendanceModal.class,context,new APIRestManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObject) {

                attendanceModalArrayList = (ArrayList<AttendanceModal>) resultObject;

                //    loader.dismissLoader();
                //  referedHospitalAdapter.updateAdapter(referedHospitalModalArrayList);
                calendarEvents();

            }

            @Override
            public void onError(String msg) {
                //   loader.dismissLoader();
                Toast.makeText(context,"Error",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
