package com.jass.e_swift.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jass.e_swift.R;
import com.jass.e_swift.activity.E_BookActivity;
import com.jass.e_swift.model.PdfModal;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PdfThambnailAdapter extends RecyclerView.Adapter<PdfThambnailAdapter.MyViewHolder> {
    Context context;
    ArrayList<PdfModal> videoModalArrayList;
    public static final String IMG_BASE_URL="http://eswift.in/ImagePdf/";

    public PdfThambnailAdapter(ArrayList<PdfModal> videoModalArrayList,Context context)
    {
        this.videoModalArrayList=videoModalArrayList;
        this.context=context;
    }
    public void updateAdapter(ArrayList<PdfModal> videoModalArrayList,Context context)
    {
        this.videoModalArrayList=videoModalArrayList;
        this.context=context;
        notifyDataSetChanged();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        return new MyViewHolder(LayoutInflater.from(viewGroup.getRootView().getContext()).inflate(R.layout.single_item_pdf_thambnail,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int i) {

        //   myViewHolder.txtVideoName.setText(videoModalArrayList.get(i).getVideo_name());
        final String url=IMG_BASE_URL.concat(videoModalArrayList.get(i).getPdf_thambnail());
        //final String url1=url.concat(".jpg");
        System.out.println("image thamb nail"+url);
        Picasso.with(context).load(url).into(myViewHolder.imgThambnail);
        myViewHolder.imgThambnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context,E_BookActivity.class).putExtra("pdf_name",videoModalArrayList.get(i).getPdf_name()));
            }
        });
        //myViewHolder
    }

    @Override
    public int getItemCount() {
        return videoModalArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        //  TextView txtVideoName;
        ImageView imgThambnail;

        public MyViewHolder(View itemView) {
            super(itemView);
            // txtVideoName=itemView.findViewById(R.id.txt_video_name);
            imgThambnail=itemView.findViewById(R.id.thambnail_img);
        }
    }
}
