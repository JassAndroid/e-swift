package com.jass.e_swift.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jass.e_swift.Dialog.ExamStatusDialog;
import com.jass.e_swift.Dialog.ExamTypeDialog;
import com.jass.e_swift.R;
import com.jass.e_swift.Utility.AppConstant;
import com.jass.e_swift.activity.E_BookActivity;
import com.jass.e_swift.activity.EbookThambnailActivity;
import com.jass.e_swift.activity.MCQActivity;
import com.jass.e_swift.activity.VideoThambnail;
import com.jass.e_swift.model.CuricularModel;
import com.jass.e_swift.model.MCQSSubjectModel;

import java.util.ArrayList;

public class MCQSSubjectAdapter extends RecyclerView.Adapter<MCQSSubjectAdapter.MyViewHolder> {
    Context context;
    ArrayList<MCQSSubjectModel> list;
    String key;

    public MCQSSubjectAdapter(Context context, ArrayList<MCQSSubjectModel> list, String key) {
        this.context = context;
        this.list = list;
        this.key = key;
    }


    public void updateAdapter(ArrayList<MCQSSubjectModel> list, String key) {
        this.list = list;
        this.key = key;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.list_mcqs_subject, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        final MCQSSubjectModel menu = list.get(position);
        myViewHolder.name.setText(menu.getContent_name());
        //   myViewHolder.image.setImageResource(menu.getImage());
        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (key.equalsIgnoreCase("mcq")) {
                    final ExamTypeDialog dlg = new ExamTypeDialog(context);
                    dlg.show();
                    dlg.btnOnlineTest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppConstant.isTest = true;
                            context.startActivity(new Intent(context, MCQActivity.class).putExtra("subject_id", menu.getID()));
                            dlg.dismiss();
                        }
                    });
                    dlg.btnPracticeTest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppConstant.isTest = false;
                            context.startActivity(new Intent(context, MCQActivity.class).putExtra("subject_id", menu.getID()));
                            dlg.dismiss();
                        }
                    });
            /*    dlg.rdo_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                    }
                });*/


                }
                if (key.equalsIgnoreCase("video")) {
                    context.startActivity(new Intent(context, VideoThambnail.class));
                }
                if (key.equalsIgnoreCase("pdf")) {
                    context.startActivity(new Intent(context, EbookThambnailActivity.class));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //  LinearLayout linr_lay;
        CardView cardView;
        TextView name;
        ImageView image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            name = itemView.findViewById(R.id.txt_macqs_subject_name);
            //    image = itemView.findViewById(R.id.img_mcqs_subject);

        }
    }
}
