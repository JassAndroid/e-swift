package com.jass.e_swift.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jass.e_swift.R;
import com.jass.e_swift.activity.E_BookActivity;

import com.jass.e_swift.activity.MCQActivity;
import com.jass.e_swift.activity.MCQSSubjectActivity;
import com.jass.e_swift.activity.StudentCurriculumActivity;
import com.jass.e_swift.activity.VideoTutorialActivity;
import com.jass.e_swift.model.CuricularModel;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    Context context;
    ArrayList<CuricularModel> list;


    public DashboardAdapter(Context context, ArrayList<CuricularModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.list_curicular,parent,false));

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        CuricularModel menu = list.get(position);
        myViewHolder.name.setText(menu.getName());
        myViewHolder.image.setImageResource(menu.getImage());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==0) {
                    Intent student_curriculum = new Intent(context, StudentCurriculumActivity.class);
                    context.startActivity(student_curriculum);
                }
                if(position==1)
                {
                    Intent mcqssubject = new Intent(context,MCQSSubjectActivity.class).putExtra("key","mcq");
                    context.startActivity(mcqssubject);
                }
                if(position==2)
                {
                    Intent student_curriculum = new Intent(context, MCQSSubjectActivity.class).putExtra("key","video");
                    context.startActivity(student_curriculum);
                }
                if(position==3)
                {
                    Intent student_curriculum = new Intent(context, MCQSSubjectActivity.class).putExtra("key","pdf");
                    context.startActivity(student_curriculum);
                }
            }
        });


    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;

        public MyViewHolder(@NonNull View itemView) {

            super(itemView);

            name = itemView.findViewById(R.id.txt_name);
            image = itemView.findViewById(R.id.img_dashboard);

        }
    }
}