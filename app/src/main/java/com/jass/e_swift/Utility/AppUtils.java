package com.jass.e_swift.Utility;

import android.content.Context;
import android.widget.Toast;

public class AppUtils {
    public static void showAlert(String msg, Context context) {
        Toast.makeText(context, msg,
                Toast.LENGTH_SHORT).show();
    }

}
