package com.jass.e_swift.Utility;

import com.jass.e_swift.model.QuestionAnswerModel;

import java.util.ArrayList;

public class AppConstant {
    public static boolean isDemo = true;
    public static ArrayList<QuestionAnswerModel> queAnsModelArray = new ArrayList<>();
    public static String selectedSubject = "";
    public static String selectedSubjectIndex = "1";
    public static String selectedLanguage = "";
    public static boolean isTest;
    public  static boolean isLoginActivity=false;
    public  static String jsonString;
    //Urls
    public static final String BASE_URL = "http://eswift.in/Eswift/E_SwiftAPI/";
    public static final String USER_LOGIN_API = BASE_URL + "login_api.php";
    public static final String CHCK_LOGIN_IN_BACKGROUND = BASE_URL +"chekLoginFromBackground.php";
    public static final String GET_SUBJECT_COURSE_CONTENT = BASE_URL +"getCoursContent.php";
    public static final String GET_MCQ_LIST = BASE_URL +"get_test_mcq.php";
}
