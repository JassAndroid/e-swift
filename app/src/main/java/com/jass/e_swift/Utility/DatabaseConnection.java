package com.jass.e_swift.Utility;

import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DatabaseConnection {
    public static Connection getSqlConnection()
    {
        Connection DbConn = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String uname = "mobile_dev";
        String pwd = "mobile_dev@123";

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            DbConn = DriverManager.getConnection("jdbc:jtds:sqlserver://198.71.225.146:1433/" +"ph13268800206_ESWIFT;encrypt=false;user=" + uname + ";password=" + pwd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DbConn;

    }
}
