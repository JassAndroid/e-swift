package com.jass.e_swift.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class Preferences {
    private final static String preferencesName = "E_Swift";
    public static Context appContext;



    public static void setLastSolvedQuestion(String keyName,int value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putInt(keyName, value);
        editor.commit();
    }

    public static int getLastSolvedQuestion(String keyName) {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        int value = prefs.getInt(keyName, 0);
        return value;
    }
    public static void setFirstTimeLoginStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getFirstTimeStatus", value);
        editor.commit();
    }

    public static boolean getFirstTimeLoginStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getFirstTimeStatus", false);
        return value;
    }}
