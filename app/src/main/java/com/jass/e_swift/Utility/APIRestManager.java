package com.jass.e_swift.Utility;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.jass.e_swift.DbHelpar.SQLHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JASS-3 on 12/14/2018.
 */

public class APIRestManager {
    private RequestQueue requestQueue;
    Context context;
    SQLHelper sqLiteHelper;

    //public  APIManagerInterface listner;

    public interface APIManagerInterface {
        public void onSuccess(Object resultObj);

        public void onError(String strError);
    }

    /*   public void postObjectAPI(final Context context, String url, final Class ClassTypye, JSONObject params, final APIManagerInterface listener) {
           if (CheckInternet.isNetworkAvailable(context)) {
               requestQueue = Volley.newRequestQueue(context);

               this.context = context;

               JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                       new Response.Listener<JSONObject>() {
                           @Override
                           public void onResponse(JSONObject response) {
                               try {
                                   JSONObject object = null;
                                   if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                       String jsonString = response.getString("response");
                                       OtpModel model = new OtpModel();
                                       Gson gson = new Gson();
                                       // model = gson.fromJson(jsonString, OtpModel.class);
                                       // object = new JSONObject(jsonString);
                                       // Object model = new Gson().fromJson(object.toString(), ClassTypye);
                                       //  Gson gson = new Gson();
                                       //
                                       model = gson.fromJson(jsonString, OtpModel.class);

                                       if (listener != null) {
                                           listener.onSuccess(model);
                                       }
                                   } else {
                                       if (listener != null) {
                                           listener.onError(object.getString("message"));
                                       }
                                   }
                               } catch (JSONException e) {
                                   e.printStackTrace();
                                   listener.onError("");
                               } catch (Exception e) {
                                   Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                               }
                           }
                       }, new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
                       if (listener != null) {
                           listener.onError("server error");
                       }
                   }
               });

               requestQueue.add(mJsonObjectRequest);
               mJsonObjectRequest.setShouldCache(false);
               mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                       3000,
                       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                       DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           } else {
               listener.onError("Please check your internet connection");
           }
       }*/
    public void postAPI(String url, JSONObject params, final Context context, final Class classType, final APIManagerInterface listner) {

        if (CheckInternet.isNetworkAvailable(context)) {
            requestQueue = Volley.newRequestQueue(context);

            this.context = context;

            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest jsonData = new JsonObjectRequest(Request.Method.POST, url, params,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            try {
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    try {
                                        Object json = response.get("response");
                                        //if multiple objects
                                        if (json instanceof JSONArray) {
                                            ArrayList resultArray = new ArrayList();
                                            JSONArray list = response.getJSONArray("response");
                                            for (int i = 0; i < list.length(); i++) {
                                                JSONObject object = list.getJSONObject(i);
                                                // list.getString(i);
                                                Object model = gson.fromJson(object.toString(), classType);
                                                resultArray.add(model);
                                                //  Gson gson = new Gson();
                                                //model = gson.fromJson(jsonResponse, LoginResponseModel.class);

                                            }
                                            if (listner != null) {
                                                listner.onSuccess(resultArray);
                                            }
                                        } else {
                                            String jsonStr = "response";
                                            Object result = null;
                                            if (response.toString().contains("No record found")) {
                                                result = "No record found";
                                            } else if (response.toString().contains("Pranjali found")) {
                                                result = "1111";
                                            } else {
                                                JSONObject jsonObject = response.getJSONObject(jsonStr);
                                                result = gson.fromJson(jsonObject.toString(), classType);
                                            }
//                                        result = ModelParser.parseModel((JSONObject) json, result);

                                            if (listner != null) {
                                                listner.onSuccess(result);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        listner.onError(e.getLocalizedMessage());
                                    }
//                                {"responce":[{"value":"1"},{"value":"1"}]}
                                } else if (response.getBoolean("responce")) {
                                    Object json = response.get("response");
                                    if (json instanceof JSONArray) {
                                        ArrayList resultArray = new ArrayList();

                                        JSONArray list = response.getJSONArray("response");
                                        for (int i = 0; i < list.length(); i++) {
                                            JSONObject object = list.getJSONObject(i);
                                            Object model = gson.fromJson(object.toString(), classType);
                                            resultArray.add(model);
                                        }
                                        if (listner != null) {
                                            listner.onSuccess(resultArray);
                                        }
                                    } else {
                                        String jsonStr = "response";
                                        Object result = null;
                                        if (response.toString().contains("No record found")) {
                                            result = "No record found";
                                        } else {
                                            JSONObject jsonObject = response.getJSONObject(jsonStr);
                                            result = gson.fromJson(jsonObject.toString(), classType);
                                        }
//                                        result = ModelParser.parseModel((JSONObject) json, result);

                                        if (listner != null) {
                                            listner.onSuccess(result);
                                        }
                                    }
                                    if (listner != null) {
                                        listner.onError("Fail ");
                                    }
                                } else if (response.getString("status").equalsIgnoreCase("Error")) {
                                    String msg = response.getString("message");
                                    if (listner != null) {
                                        listner.onError(msg);
                                    }
                                } else if (response.getString("status").equalsIgnoreCase("Fail")) {
                                    if (!response.getBoolean("response")) {
                                        String msg = response.getString("message");
                                        if (listner != null) {
                                            listner.onError(msg);
                                        }
                                    } else {
                                        //response
                                        if (listner != null) {
                                            listner.onError("no user found");
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listner != null) {
                                    listner.onError("in catch");
                                }
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //AppUtils.showAlert(error.getLocalizedMessage(),context);
                    if (listner != null) {
                        listner.onError(error.toString());
                    }
                }
            });
            requestQueue.add(jsonData);
            jsonData.setShouldCache(false);
            jsonData.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }else {
            Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void postJSONArrayAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (CheckInternet.isNetworkAvailable(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("response");

                                    JSONArray jsonArray = new JSONArray(jsonString);
                                    ArrayList arrList = new ArrayList();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        Object model = gson.fromJson(object.toString(), classType);
                                        arrList.add(model);
                                    }

                                    if (listener != null) {
                                        listener.onSuccess(arrList);
                                    }


                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("message"));
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("message"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("message"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        listener.onError(error.toString());
                    }
                }
            });

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    3000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            if (listener != null) {
//                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }




    public void postObjectAPI(final Context context, String url, final Class classTypye, JSONObject params, final APIManagerInterface listener) {
        if (CheckInternet.isNetworkAvailable(context)) {
            requestQueue = Volley.newRequestQueue(context);

            this.context = context;

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                              /*  JSONObject object = null;
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    String jsonString = response.getString("response");

                                  // if (ClassTypye == null)
                                  //  {
                                       // listener.onSuccess(null);
                                 //   }

                                    UserRegistrationModel model = new UserRegistrationModel();
                                    Gson gson = new Gson();
                                    // model = gson.fromJson(jsonString, OtpModel.class);
                                    // object = new JSONObject(jsonString);
                                    // Object model = new Gson().fromJson(object.toString(), ClassTypye);
                                    //  Gson gson = new Gson();
                                    //
                                    model = gson.fromJson(jsonString, UserRegistrationModel.class);*/
                                if (response.getString("status").equalsIgnoreCase("success")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("response");
                                    Log.d("response =",response+"");

                                    JSONObject object = new JSONObject(jsonString);
                                    Object model = gson.fromJson(object.toString(), classTypye);

                                    if (listener != null) {
                                        System.out.println("jsonstring="+jsonString);
                                      if(AppConstant.isLoginActivity)
                                      {
                                          AppConstant.jsonString=jsonString;

                                      }
                                            listener.onSuccess(model);

                                    }
                                } else {
                                    if (listener != null) {

                                        listener.onError(response.getString("message"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                listener.onError("");
                            } catch (Exception e) {
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                                Log.d("error",e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        listener.onError("server error");
                    }
                }
            });

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            listener.onError("Please check your internet connection");
            Toast.makeText(context,"Please check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
}
