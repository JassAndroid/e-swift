package com.jass.e_swift.Utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;

import com.jass.e_swift.R;

public class CustomLoader {
    private Context context;
    private ProgressDialog progress;


    public CustomLoader(Context context) {
        this.context = context;
    }

    public void showLoader() {
        progress = ProgressDialog.show(context, null, null, true);
        progress.setContentView(R.layout.layout_custom_loader);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        progress.show();
    }

    public void dismissLoader() {
        progress.dismiss();
    }
}